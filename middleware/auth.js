const jwt = require('jsonwebtoken');

const userJSON = require('../json/user.json');

jwtToken = process.env.TOKEN;

const auth = async (req, res, next) => {
	try {
		const token = req.header('Authorization');
		const decoded = jwt.verify(token, jwtToken);
		const user = userJSON.filter((val) => val.id == decoded._id);
		if (user[0].id == decoded._id) {
			req.token = token;
			req.user = user[0];
			next();
		}
		if (!user) {
			res.send(err);
			throw err;
		}
	} catch (e) {
		res.status(401).send({ error: 'Please Authenticate' });
	}
};
module.exports = auth;
