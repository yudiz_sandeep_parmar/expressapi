const jwt = require('jsonwebtoken');
const validator = require('../middleware/validator');
const fs = require('fs');
var crypto = require('crypto');
const userController = {};
const userJSON = require('../json/user.json');

userController.register = async (req, res) => {
	try {
		validator.error(req, res);
		if (userJSON.some((val) => val.token.length != 0)) {
			return res.status(200).json({ message: 'please sign out' });
		}
		if (userJSON.some((val) => val.email == req.body.email)) {
			return res.status(200).json({ message: 'duplicate email' });
		}
		const newId = crypto.randomBytes(5).toString('hex');
		let userData = {
			id: newId,
			token: jwt.sign({ _id: newId }, 'token'),
			...req.body,
		};
		userJSON.push(userData);
		fs.writeFile(
			'../expressAPI/json/user.json',
			JSON.stringify(userJSON),
			(err) => {
				if (err) return res.send(err);
			}
		);
		res
			.status(201)
			.json({ message: 'successfully registered', token: userData.token });
	} catch (error) {
		res.status(400).json({ message: error });
	}
};

userController.login = async (req, res) => {
	try {
		validator.error(req, res);
		if (userJSON.some((val) => val.token.length != 0)) {
			return res.status(200).json({ message: 'please sign out' });
		}
		const user = userJSON.filter((val) => val.email == req.body.email);
		if (user == '') {
			res.status(401).json({ message: 'Please register' });
		} else if (user[0].password == req.body.password) {
			token = jwt.sign({ _id: user[0].id }, 'token');
			const usersData = userJSON.map((val) =>
				val.id == user[0].id
					? {
							...val,
							token: token,
					  }
					: val
			);
			fs.writeFile(
				'../expressAPI/json/user.json',
				JSON.stringify(usersData),
				(err) => {
					if (err) return res.send(err);
				}
			);
			res.status(201).json({ message: 'Logged in!', token });
		} else {
			res.status(400).json({ message: 'email or password must be wrong!!!' });
		}
	} catch (e) {
		console.log('====================================');
		console.log(e);
		console.log('====================================');
		res.status(400).json({ message: e });
	}
};

userController.editProfile = async (req, res) => {
	try {
		const { id } = req.user;
		validator.error(req, res);
		const authEmail = req.user.email;
		if (authEmail != req.body.email) {
			if (userJSON.some((val) => val.email == req.body.email)) {
				return res.status(200).json({ message: 'duplicate email' });
			}
		}
		const usersData = userJSON.map((val) =>
			val.id == id
				? {
						...val,
						email: req.body.email,
						userName: req.body.userName,
				  }
				: val
		);
		fs.writeFile(
			'../expressAPI/json/user.json',
			JSON.stringify(usersData),
			(err) => {
				if (err) return res.json({ message: err });
			}
		);
		res.status(200).json({ message: 'Data Updated' });
	} catch (e) {
		res.status(400).json({ message: error });
	}
};

userController.changePassword = async (req, res) => {
	try {
		validator.error(req, res);
		const { id } = req.user;
		const usersData = userJSON.map((val) =>
			val.id == id
				? {
						...val,
						password: req.body.newPassword,
				  }
				: val
		);
		if (req.body.newPassword === req.body.confirm) {
			fs.writeFile(
				'../expressAPI/json/user.json',
				JSON.stringify(usersData),
				(err) => {
					if (err) return res.json({ message: err });
				}
			);
			res.status(200).json({ message: 'Password Updated' });
		} else {
			res.status(400).send({ message: 'password not matched' });
		}
	} catch (e) {
		res.status(400).json({ message: error });
	}
};

userController.logout = async (req, res) => {
	try {
		const { id } = req.user;
		const removeToken = userJSON.map((user) =>
			user.id == id ? { ...user, token: '' } : user
		);
		fs.writeFile(
			'../expressAPI/json/user.json',
			JSON.stringify(removeToken),
			(err) => {
				if (err) return res.json({ message: err });
			}
		);
		res.status(200).json({ message: 'logged out!' });
	} catch (e) {
		res.status(400).json({ message: e });
	}
};

module.exports = userController;
