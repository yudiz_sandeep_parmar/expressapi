const validator = require('../middleware/validator');
const fs = require('fs');
var crypto = require('crypto');
const orderController = {};
const stockJSON = require('../json/stock.json');
const orderJSON = require('../json/order.json');

orderController.itemList = async (req, res) => {
	try {
		res.status(200).send(stockJSON);
	} catch (e) {
		res.status(400).json({ message: e });
	}
};

orderController.order = async (req, res) => {
	try {
		validator.error(req, res);
		const { id } = req.user;
		if (stockJSON.some((i) => req.body.item == i.item && i.quantity > 0)) {
			const updateStock = stockJSON.map((val) =>
				val.item == req.body.item
					? { ...val, quantity: parseInt(val.quantity - 1) }
					: val
			);
			const orderData = {
				orderId: crypto.randomBytes(5).toString('hex'),
				userId: id,
				item: req.body.item,
			};
			orderJSON.push(orderData);
			fs.writeFile(
				'../expressAPI/json/order.json',
				JSON.stringify(orderJSON),
				(err) => {
					if (err) return res.json(err);
				}
			);
			fs.writeFile(
				'../expressAPI/json/stock.json',
				JSON.stringify(updateStock),
				(err) => {
					if (err) return res.json({ message: 'something went wrong' });
				}
			);
			res.status(201).json({ message: 'order placed' });
		} else {
			res.status(204).json({ message: 'not in stocked or invalid order item' });
		}
	} catch (e) {
		res.status(404).json({ e });
	}
};

orderController.getUserOrders = async (req, res) => {
	try {
		const { id } = req.user;
		const userOrders = orderJSON.filter((o) => o.userId == id);
		res.status(200).json(userOrders);
	} catch (e) {
		res.status(404).json({ e });
	}
};

module.exports = orderController;
