const serverController = {};

serverController.server = async (req, res) => {
	try {
		res.json({ response: 'Server pinged!' });
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

serverController.wildCard = async (req, res) => {
	try {
		res.json({ message: 'Route not found!' });
	} catch (error) {
		res.status(404).json({ message: error.message });
	}
};

module.exports = serverController;
