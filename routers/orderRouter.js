const express = require('express');
const router = new express.Router();
const { body } = require('express-validator');
const auth = require('../middleware/auth');
const orderController = require('../controllers/orderController');

router.get('/list', auth, orderController.itemList);

router.post(
	'/order',
	body('item').not().isEmpty(),
	auth,
	orderController.order
);

router.get('/userOrder', auth, orderController.getUserOrders);

module.exports = router;
