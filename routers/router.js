const userRouter = require('./userRouter');
const orderRouter = require('./orderRouter');
const serverController = require('../controllers/serverController');
const express = require('express');

const router = new express.Router();

router.use(userRouter);
router.use(orderRouter);

router.get('/', serverController.server);

router.all('*', serverController.wildCard);

module.exports = router;
