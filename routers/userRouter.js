const express = require('express');
const router = new express.Router();
const auth = require('../middleware/auth');
const { body } = require('express-validator');
const userController = require('../controllers/userController');

router.post(
	'/users/reg',
	body('email').isEmail().not().isEmpty(),
	body('userName').not().isEmpty(),
	body('password').isLength({ min: 6 }).not().isEmpty(),
	userController.register
);

router.post(
	'/users/login',
	body('email').isEmail().not().isEmpty(),
	body('password').isLength({ min: 6 }).not().isEmpty(),
	userController.login
);

router.patch(
	'/users/edit/',
	auth,
	body('email').isEmail().not().isEmpty(),
	body('userName').not().isEmpty().not().isEmpty(),
	userController.editProfile
);

router.patch(
	'/users/password/',
	auth,
	body('current').not().isEmpty(),
	body('newPassword').not().isEmpty(),
	body('confirm').not().isEmpty(),
	userController.changePassword
);

router.get('/users/logout', auth, userController.logout);

module.exports = router;
