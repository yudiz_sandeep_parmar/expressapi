const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const router = require('../routers/router');

const app = express();

app.use(helmet());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors({ origin: '*', exposedHeaders: ['Content-Range'] }));
app.use(router);
app.listen(process.env.PORT || 3000, () => {
	console.log('Server started at port: ' + process.env.PORT);
});
